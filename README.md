# My text game 

Main.cpp
#include <iostream>
#include <vector>
#include <string>
#include "Room.h"
#include "GameObject.h"
#include "Inventory.h"
#include "Item.h"

// Define the Player class
class Player {
private:
    std::string name;
    int health;
    std::vector<GameObject*> inventory;
    Room* location; // Pointer to the current room
public:
    Player(const std::string& playerName, int startingHealth, Room* startingLocation)
        : name(playerName), health(startingHealth), location(startingLocation) {}

    std::string getName() const { return name; }
    int getHealth() const { return health; }
    Room* getLocation() const { return location; }

    void takeDamage(int damage) { health -= damage; }

    void addItemToInventory(GameObject* item) { inventory.push_back(item); }

    void displayInventory() const {
        std::cout << "Inventory: ";
        for (const auto& item : inventory) {
            std::cout << item->getName() << " ";
        }
        std::cout << std::endl;
    }
};

// Function to explore rooms
void exploreRooms(const std::vector<Room>& rooms) {
    std::cout << "Rooms:" << std::endl;
    for (size_t i = 0; i < rooms.size(); ++i) {
        std::cout << i + 1 << ". " << rooms[i].getName() << ": " << rooms[i].getDescription() << std::endl;
    }
}

// Function to pick a room and see the item inside
void pickRoom(std::vector<Room>& rooms, Inventory& inventory, Player& player) {
    std::cout << "Rooms:" << std::endl;
    for (size_t i = 0; i < rooms.size(); ++i) {
        std::cout << i + 1 << ". " << rooms[i].getName() << std::endl;
    }

    int roomNumber;
    std::cout << "Enter the room number to see the item inside: ";
    std::cin >> roomNumber;
    roomNumber--; // Adjust for 0-based indexing

    if (roomNumber >= 0 && roomNumber < rooms.size()) {
        GameObject* item = rooms[roomNumber].getItem();
        if (item != nullptr) {
            std::cout << "Items in " << rooms[roomNumber].getName() << ":" << std::endl;
            inventory.addItem(item);
            inventory.displayItems();
            if (item->getName() == "Coin") {
                std::cout << "Congratulations! You found the coin and WIN!" << std::endl;
                std::cout << "Main Menu:" << std::endl;
                std::cout << "1. Start New Game\n2. Quit\n";
                int choice;
                std::cin >> choice;
                if (choice == 1) {
                    inventory.clearItems(); // Clear items from previous game
                    return; // Start new game
                } else if (choice == 2) {
                    std::cout << "Goodbye!" << std::endl;
                    exit(0); // Quit the game
                }
            } else {
                std::cout << "You found an item, but it's not the coin." << std::endl;
            }
        } else {
            std::cout << "No item found in this room." << std::endl;
        }
    } else {
        std::cout << "Invalid room number!" << std::endl;
    }
}

// Function to move to another room
void moveRoom(Player& player) {
    std::string direction;
    std::cout << "Enter the direction (e.g., north, south): ";
    std::cin >> direction;
    if (direction == "north") {
        std::cout << "You move to the next room." << std::endl;
    } else if (direction == "south") {
        std::cout << "You can't go that way." << std::endl;
    } else {
        std::cout << "Invalid direction!" << std::endl;
    }
}

int main() {
    std::vector<Room> rooms;
    Inventory inventory;

    rooms.push_back(Room("Secret Path", "A hidden pathway winding through the darkness, its secrets waiting to be discovered.", new Coin()));
    rooms.push_back(Room("Spooky Loft", "An eerie attic filled with cobwebs and mysterious shadows lurking in the corners.", new Food()));
    rooms.push_back(Room("Dimly Lit Room", "A room barely illuminated by flickering torches, casting eerie shadows on the walls.", nullptr));
    rooms.push_back(Room("Long Hallway", "A seemingly endless corridor stretching into the distance, its length shrouded in darkness.", nullptr));

    std::cout << "Welcome to the adventure! Find the coin to WIN." << std::endl;

    // Create a player
    Player player("Alice", 100, &rooms[0]); // Assuming starting location is the first room

    int choice;
    do {
        std::cout << "Main Menu:" << std::endl;
        std::cout << "1. Explore room\n2. Pick a room to see the item inside\n3. Move to another room\n4. Quit" << std::endl;
        std::cin >> choice;

        switch (choice) {
            case 1:
                exploreRooms(rooms);
                break;
            case 2:
                pickRoom(rooms, inventory, player);
                break;
            case 3:
                moveRoom(player);
                break;
            case 4:
                std::cout << "Goodbye!" << std::endl;
                break;
            default:
                std::cout << "Invalid choice!" << std::endl;
        }
    } while (choice != 4);

    return 0;
}

GameObject.h
#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <string>

class GameObject {
protected:
    std::string name;
public:
    GameObject(const std::string& n) : name(n) {}
    virtual ~GameObject() {}
    virtual void describe() const = 0;
    std::string getName() const { return name; }
};

#endif // GAMEOBJECT_H

Inventory.cpp
#include "Inventory.h"

Inventory::~Inventory() {
    for (auto item : items) {
        delete item;
    }
}

void Inventory::addItem(GameObject* item) {
    items.push_back(item);
}

void Inventory::displayItems() const {
    for (const auto& item : items) {
        item->describe();
    }
}

bool Inventory::hasItem(const std::string& itemName) const {
    for (const auto& item : items) {
        if (item->getName() == itemName) {
            return true;
        }
    }
    return false;
}

void Inventory::clearItems() {
    for (auto item : items) {
        delete item;
    }
    items.clear();
}

Inventory.h
#ifndef INVENTORY_H
#define INVENTORY_H

#include <vector>
#include <string>
#include "GameObject.h"

class Inventory {
private:
    std::vector<GameObject*> items; // Use pointers for polymorphic behavior
public:
    ~Inventory();

    void addItem(GameObject* item);
    void displayItems() const;
    bool hasItem(const std::string& itemName) const;
    void clearItems();
};

#endif // INVENTORY_H

Item.cpp
// Item.cpp
#include "Item.h"
#include <iostream>

Coin::Coin() : GameObject("Coin") {}

void Coin::describe() const {
    std::cout << "- Coin: A gleaming gold coin, the ultimate prize!" << std::endl;
}

Food::Food() : GameObject("Food") {}

void Food::describe() const {
    std::cout << "- Food: A delicious meal that could replenish your strength." << std::endl;
}

Key::Key() : GameObject("Key") {}

void Key::describe() const {
    std::cout << "- Key: A mysterious key that may unlock hidden secrets." << std::endl;
}

Map::Map() : GameObject("Map") {}

void Map::describe() const {
    std::cout << "- Map: A detailed map of the surrounding area, showing hidden paths and treasures." << std::endl;
}

Item.h 
// Item.h
#ifndef ITEM_H
#define ITEM_H

#include "GameObject.h"

class Coin : public GameObject {
public:
    Coin();
    void describe() const override;
};

class Food : public GameObject {
public:
    Food();
    void describe() const override;
};

class Key : public GameObject {
public:
    Key();
    void describe() const override;
};

class Map : public GameObject {
public:
    Map();
    void describe() const override;
};

#endif // ITEM_H

Room.h 
#include "Room.h"

Room::Room(const std::string& n, const std::string& desc, GameObject* obj) : name(n), description(desc), item(obj) {}

std::string Room::getName() const {
    return name;
}

std::string Room::getDescription() const {
    return description;
}

GameObject* Room::getItem() const {
    return item;
}

Room.h 
#ifndef ROOM_H
#define ROOM_H

#include <string>
#include "GameObject.h"

class Room {
private:
    std::string name;
    std::string description;
    GameObject* item;
public:
    Room(const std::string& n, const std::string& desc, GameObject* obj);

    std::string getName() const;
    std::string getDescription() const;
    GameObject* getItem() const;
};

#endif // ROOM_H
